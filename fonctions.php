<?php

function toTable($type,$sth,$args=array(),$linkable=false)
{
	//echo "Liste des ".$type."";
	//creation du tableau
	echo '<table class="w3-table w3-margin-top">
	      <tr class="w3-blue-grey">';
	foreach ($args as $a) {
		echo "<th>".$a."</th>\n";
	}
	echo "<th> </th>\n";
	echo "</tr>\n";
	//affichage les data dans le tableau
	while($result = $sth->fetch(PDO::FETCH_NUM)){ // tant que on a une valeur qui correspond a la requete
		echo '<tr class="w3-hover-black w3-gray">';
  		for ($i=0; $i < sizeof($result) ; $i++) { 
  				echo "<td>".$result[$i]."</td>\n";
  		}
  		if ($linkable) {
  			echo "<td>".toLink($type,$result)."</td>\n";
  		}
  		echo "</tr>\n";
	}
	echo "</table>";
}
function toCard($sth,$args=array())
{
	$result = $sth->fetch(PDO::FETCH_NUM);
	$description.='<table class="w3-table w3-margin-bottom">';
	for ($i=1; $i < sizeof($args); $i++) { 
			$description.='<tr class="w3-hover-blue-grey w3-light-gray"><td>'.$args[$i].'</td><td>'.$result[$i].'</td></tr>';
	}
	$description.='</table>';

    echo'<center><div class="w3-card w3-margin-top w3-grey" style="width:50%">
    		<img src="Draw_book.png" alt="book" style="width:50%">
    		<div class="w3-container">
      		<h4><b>'.$result[0].'</b></h4>
      		'.$description.'
    		</div>
  		</div></center>';

	
	//echo "<td>".toLink("Supprimer")."</td>\n";

}

function toLink($type,$args=array())
{
	$get = "";
	if ($type=="Manga") {
		$link = '<a href="page_manga.php?Id='.$args[0].'">afficher</a>';
	}else{	
		if ($type=="Dessinateur"Or $type=="Auteur") {
			$get.="Nom=".$args[0]."&";
			$get.="Prenom=".$args[1]."";
		}else{
			$get ="Name=".$args[0];
		}
		$link = '<a href="liste_mangas.php?type='.$type.'&'.$get.'">afficher</a>';
	}
	return $link;

}

function initheader()
{
	echo '<!DOCTYPE html>
<html>
<title>Mangatech</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="W3.css">
<link rel="icon" href="Draw_book.png">
<body class="w3-dark-grey">
<div class="w3-bar w3-black">
	<a href="index.php" class="w3-bar-item w3-button">Home</a>
	<a href="creationBD.php" class="w3-bar-item w3-button"> Initialisation SQlite</a>
	<a href="MysqlCredits.php" class="w3-bar-item w3-button"> Initialisation MySQL</a>
	<a href="liste_mangas.php" class="w3-bar-item w3-button"> Liste des mangas</a>
	<a href="listes.php?type=Auteur" class="w3-bar-item w3-button"> Liste des auteurs</a>
	<a href="listes.php?type=Dessinateur" class="w3-bar-item w3-button"> Liste des dessinateurs</a>
	<a href="listes.php?type=Genre" class="w3-bar-item w3-button"> Liste des genres</a>
	<a href="listes.php?type=Annee" class="w3-bar-item w3-button"> Liste des années</a>
	<a href="addBD.php" class="w3-bar-item w3-button"> Ajouter un manga </a>
	<a href="deleteBD.php" class="w3-bar-item w3-button"> Supprimer un manga </a>
</div>

<div class="w3-container w3-middle">';
}

function initfooter()
{
	echo '
	</div>
	<div class="w3-bar w3-black w3-bottom">
  <div class="w3-bar-item w3-left">©Mangatech-2021</div>
  <div class="w3-bar-item w3-right ">Made by Luca FAUBOURG and Jules HENRIETTE</div>
</div>
</body>
</html> ';
}
function quote($text)
{
  echo '<div class="w3-container w3-middle">
  <div class="w3-panel w3-light-grey">
  <span style="font-size:150px;line-height:0.6em;opacity:0.2">❝</span>
  <p class="w3-xlarge" style="margin-top:-40px"><i>'.$text.'</i></p>
  </div></div>';
}

function alert($type,$text){
	if ($type=="success") {
		echo '
		<div class="w3-panel w3-light-green w3-card-4">
  	<h3>Success!</h3>
  	<p>'.$text.'.</p>
		</div>';
	}else{
		echo '
		<div class="w3-panel w3-yellow w3-card-4">
  	<h3>Warning!</h3>
  	<p>'.$text.'.</p>
		</div>';
	}
}

function form($title,$method,$action,$value,$listeAttr=array(),$listeValues=NULL)
{
	echo "<div class='w3-container w3-blue-grey w3-margin-bottom w3-margin-top'><h3>".$title."</h3></div>";
	echo "<form method='".$method."' action='".$action."' class='w3-container'>";
	if ($listeValues<>NULL) {
		echo "<label>{$listeAttr[0]->get_text()}</label>";
   	echo "<br/><input class='w3-input' type='{$listeAttr[0]->get_type()}' value='{$listeValues[0]}' disabled='' name='{$listeAttr[0]->get_name()}'><br>";
	}else{
  	echo "<label>{$listeAttr[0]->get_text()}</label>";
    echo "<br/><input class='w3-input' type='{$listeAttr[0]->get_type()}' name='{$listeAttr[0]->get_name()}' ><br>";
	}
	for ($i = 1;$i<sizeof($listeAttr);$i++) {
		if ($listeValues<>NULL) {
			echo "<label>{$listeAttr[$i]->get_text()}</label>";
    	echo "<br/><input class='w3-input' type='{$listeAttr[$i]->get_type()}' value='{$listeValues[$i]}'' name='{$listeAttr[$i]->get_name()}'><br>";
		}else{
    echo "<label>{$listeAttr[$i]->get_text()}</label>";
    echo "<br/><input class='w3-input' type='{$listeAttr[$i]->get_type()}' name='{$listeAttr[$i]->get_name()}'><br>";
		}
  }
  echo "<input class='w3-btn w3-blue-grey w3-round-medium'type='submit' value='".$value."'></form>";
}
function bouton($action,$value)
{
	echo "<center><form method='POST' action='".$action."' class='w3-container w3-margin-top'>";
  echo "<input class='w3-btn w3-blue-grey w3-round-medium'type='submit' value='".$value."'></form></center>";
}

function getMangaFromId($id){
	require("connexion.php");
	$file_db = connect_bd();
	$file_db -> setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_WARNING);
	$sql = "SELECT IDmanga,Titre,NomAuteur,PrenomAuteur,NomDessinateur,PrenomDessinateur,Genre,Annee FROM MANGA WHERE IDmanga=:id";
	$sth = $file_db->prepare($sql);
	$sth->bindParam(':id', $id);
	$sth->execute();
	$result = $sth->fetch(PDO::FETCH_NUM);
	$attr = array();
	for ($i=0; $i < sizeof($result) ; $i++) { 
		array_push($attr,$result[$i]);
	}
	$file_db = null;
	return $attr;
}

/*
function update($attr)
{
	require("connexion.php");
	$file_db = connect_bd();
	$file_db -> setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_WARNING);

	$sql = "UPDATE MANGA SET Titre=:1,NomAuteur=:2,PrenomAuteur=:3,NomDessinateur=:4,PrenomDessinateur=:5,Genre=:6,Annee=:7 WHERE IDmanga=:id";
	$sth = $file_db->prepare($sql);
	for ($i=1; $i < 8; $i++) { 
		$sth->bindParam(':'.$i, $attr[$i]);
	}
	$sth->bindParam(':id', $attr[$i]);

	$sth->execute();

	$file_db = null;
}
*/

function update($attr)
{
	require("connexion.php");
	$file_db = connect_bd();
	$file_db -> setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_WARNING);

	$sql = "UPDATE MANGA SET Titre=:t,NomAuteur=:na,PrenomAuteur=:pa,NomDessinateur=:nd,PrenomDessinateur=:pd,Genre=:g,Annee=:a WHERE IDmanga=:id";
	$sth = $file_db->prepare($sql);

	$sth->bindParam(':t', $attr[1]);
	$sth->bindParam(':na', $attr[2]);
	$sth->bindParam(':pa', $attr[3]);
	$sth->bindParam(':nd', $attr[4]);
	$sth->bindParam(':pd', $attr[5]);
	$sth->bindParam(':g', $attr[6]);
	$sth->bindParam(':a', $attr[7]);
	$sth->bindParam(':id', $attr[0]);

	$sth->execute();

	$file_db = null;
}
?>