<?php

class Attribut {

    // Properties
    public $name;
    public $type;
    public $text;

    // Constructor
    public function __construct($name, $type, $text) {
        $this->name = $name;
        $this->type = $type;
        $this->text = $text;
    }

    // public function by default
    // Methods

    public function set_name($name) {
        $this->name = $name;
    }

    public function get_name() {
        return $this->name;
    }

    public function set_type($type) {
        $this->type = $type;
    }

    public function get_type() {
        return $this->type;
    }
    
    public function set_text($text) {
        $this->text = $text;
    }

    public function get_text() {
        return $this->text;
    }

}


?>