<?php
require("connexion.php");
class Requete {
    // Properties
    public $titre;
    public $nomAuteur;
    public $prenomAuteur;
    public $nomDessinateur;
    public $prenomDessinateur;
    public $genre;
    public $annee;
    public $tab;

    // Constructor
    public function __construct($titre="", $nomAuteur=NULL, $prenomAuteur=NULL, $nomDessinateur=NULL, $prenomDessinateur=NULL, $genre=NULL, $annee=NULL) {
        $this->titre = $titre;
        $this->nomAuteur = $nomAuteur;
        $this->prenomAuteur = $prenomAuteur;
        $this->nomDessinateur = $nomDessinateur;
        $this->prenomDessinateur = $prenomDessinateur;
        $this->genre = $genre;
        $this->annee = $annee;
        $this->tab = array();
    }

    // public function by default
    // Methods

    public function set_titre($titre) {
        $this->titre = $titre;
    }

    public function get_titre() {
        return $this->titre;
    }

    public function set_nomAuteur($nomAuteur) {
        $this->nomAuteur = $nomAuteur;
    }

    public function get_nomAuteur() {
        return $this->nomAuteur;
    }
    
    public function set_prenomAuteur($prenomAuteur) {
        $this->prenomAuteur = $prenomAuteur;
    }

    public function get_prenomAuteur() {
        return $this->prenomAuteur;
    }
    
    public function set_nomDessinateur($nomDessinateur) {
        $this->nomDessinateur = $nomDessinateur;
    }

    public function get_nomDessinateur() {
        return $this->nomDessinateur;
    }
    
    public function set_prenomDessinateur($prenomDessinateur) {
        $this->prenomDessinateur = $prenomDessinateur;
    }

    public function get_prenomDessinateur() {
        return $this->prenomDessinateur;
    }
    
    public function set_genre($genre) {
        $this->genre = $genre;
    }

    public function get_genre() {
        return $this->genre;
    }
    
    public function set_annee($annee) {
        $this->annee = $annee;
    }

    public function get_annee() {
        return $this->annee;
    }

    public function get_tab() {
        return $this->tab;
    }

    public function add_tab($v){
        array_push($this->tab, $v);
    }

    
    public function insertion() {
        global $idManga, $idAuteur, $idDessinateur, $idGenre, $listeInsert;
        
        $file_db = connect_bd();
        $file_db -> setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_WARNING);
        $tab = array();
        $idManga = $file_db->query('SELECT max(IDmanga) + 1 from MANGA');
        $result = $idManga->fetch(PDO::FETCH_NUM);
        array_push($tab, $result[0]);

        $idAuteur = $file_db->query('SELECT max(IDauteur) + 1 from AUTEUR');
        $result = $idAuteur->fetch(PDO::FETCH_NUM);
        array_push($tab, $result[0]);

        $idDessinateur = $file_db->query('SELECT max(IDdessinateur) + 1 from DESSINATEUR');
        $result = $idDessinateur->fetch(PDO::FETCH_NUM);
        array_push($tab, $result[0]);

        $idGenre = $file_db->query('SELECT max(IDgenre) + 1 from GENRE');
        $result = $idGenre->fetch(PDO::FETCH_NUM);
        array_push($tab, $result[0]);
    
        $insert = 'INSERT INTO MANGA (IDmanga, Titre, NomAuteur, PrenomAuteur, IDauteur, NomDessinateur, PrenomDessinateur, IDdessinateur, Genre, IDgenre, Annee) VALUES (:IDmanga, :Titre, :NomAuteur, :PrenomAuteur, :IDauteur, :NomDessinateur, :PrenomDessinateur, :IDdessinateur, :Genre, :IDgenre, :Annee)';
        $sth = $file_db->prepare($insert);
        // On lie les parametres aux variables
        $sth->bindParam(':IDmanga', $tab[0]);
        $sth->bindParam(':Titre', $this->titre);
        $sth->bindParam(':NomAuteur', $this->nomAuteur);
        $sth->bindParam(':PrenomAuteur', $this->prenomAuteur);
        $sth->bindParam(':IDauteur', $tab[1]);
        $sth->bindParam(':NomDessinateur', $this->nomDessinateur);
        $sth->bindParam(':PrenomDessinateur', $this->prenomDessinateur);
        $sth->bindParam(':IDdessinateur', $tab[2]);
        $sth->bindParam(':Genre', $this->genre);
        $sth->bindParam(':IDgenre', $tab[3]);
        $sth->bindParam(':Annee', $this->annee);
    
        $sth->execute();

        $insert = 'INSERT INTO AUTEUR (IDauteur, Nom, Prenom) VALUES (:IDauteur, :Nom, :Prenom)';
        $sth = $file_db->prepare($insert);
        // On lie les parametres aux variables
        $sth->bindParam(':IDauteur', $tab[1]);
        $sth->bindParam(':Nom', $this->nomAuteur);
        $sth->bindParam(':Prenom', $this->prenomAuteur);
    
        $sth->execute();

        $insert = 'INSERT INTO DESSINATEUR (IDdessinateur, Nom, Prenom) VALUES (:IDdessinateur, :Nom, :Prenom)';
        $sth = $file_db->prepare($insert);
        // On lie les parametres aux variables
        $sth->bindParam(':IDdessinateur', $tab[2]);
        $sth->bindParam(':Nom', $this->nomDessinateur);
        $sth->bindParam(':Prenom', $this->prenomDessinateur);
    
        $sth->execute();

        $insert = 'INSERT INTO GENRE (IDgenre, Nom) VALUES (:IDgenre, :Nom)';
        $sth = $file_db->prepare($insert);
        // On lie les parametres aux variables
        $sth->bindParam(':IDgenre', $tab[3]);
        $sth->bindParam(':Nom', $this->genre);

        
        $sth->execute();

        $file_db = null;
    }

    public function get_liste_titre() {
        $file_db = connect_bd();
        $file_db -> setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_WARNING);

        $tab = array();
        $sql = $file_db->query("SELECT Titre FROM MANGA");
        while($result = $sql->fetch(PDO::FETCH_NUM)){ // tant que on a une valeur qui correspond a la requete
            for ($i=0; $i < sizeof($result) ; $i++) { 
                array_push($tab, $result[0]);
            }
        }
        return $tab;
    }

    public function delete() {
        $file_db = connect_bd();
        $file_db -> setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_WARNING);

        $sql = "DELETE FROM MANGA 
        WHERE Titre=:titre";
        $sth = $file_db->prepare($sql);
        $sth->bindParam(':titre', $this->titre);

        $sth->execute();

        $file_db = null;

    }
    
}

?>