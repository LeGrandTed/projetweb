
<?php
date_default_timezone_set('Europe/Paris');
try{
	require("connexion.php");
	require("fonctions.php");
	initheader();
	$file_db=connect_bd();
	$file_db->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_WARNING);
	if ($_GET){
		$where = "";
		$type = $_GET['type'];
		if ($type =="Dessinateur" Or $type=="Auteur") {
			$where .= 'Nom'.$type.'=="'.$_GET['Nom'].'" and Prenom'.$type.'=="'.$_GET['Prenom'].'"'; 
		}elseif ($type=="Annee") {
			$where .=$type.'=="'.(int)$_GET['Name'].'"';
		}else{
			$where .=$type.'=="'.$_GET['Name'].'"';
		}
		$sth=$file_db->query('SELECT DISTINCT IDmanga,Titre,NomAuteur,PrenomAuteur,NomDessinateur,PrenomDessinateur,Genre,Annee FROM MANGA WHERE '.$where.' ');
	}else{
		$sth=$file_db->query('SELECT DISTINCT IDmanga,Titre,NomAuteur,PrenomAuteur,NomDessinateur,PrenomDessinateur,Genre,Annee FROM MANGA');
	}
	$args = array('ID','Titre','NomAuteur','PrenomAuteur','NomDessinateur','PrenomDessinateur','Genre','Annee');

	toTable("Manga",$sth,$args,TRUE);
  // on ferme la connexion
  $file_db=null;
	initfooter();
}
catch(PDOException $ex){
  echo $ex->getMessage();
}
	initfooter();
?>