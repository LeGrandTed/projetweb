<?php
require("fonctions.php");
include("Requete.php");
include("Attribut.php");
initheader();

$attr = new Attribut ("titre", "text", "Titre");

if ($_SERVER['REQUEST_METHOD'] == 'GET'){
    form("Suppression dans la BD","POST","deleteBD.php","Supprimer",$listeAttr=array($attr));
}
else {
    $a = $_POST[$attr->get_name()];
    $req = new Requete($a);
    $tab = $req->get_liste_titre();
    if (in_array($a, $tab)) {
        $req->delete();
        alert("success","Le Manga à bien été supprimé");
    }
    else alert("warning","Le Manga que vous voulez supprimer n'existe pas...");
}
initfooter();

?>