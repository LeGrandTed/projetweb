<?php
date_default_timezone_set('Europe/Paris');
try{
	require("connexion.php");
	require("fonctions.php");
	initheader();
	$file_db=connect_bd();
	$file_db->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_WARNING);
	if ($_GET) {
		$type = $_GET['type'];
		if ($type=="Auteur"){
			$sth=$file_db->query('SELECT DISTINCT Nom,Prenom FROM AUTEUR ORDER BY Nom');
			$args = array('Nom','Prenom');
		}elseif ($type=="Dessinateur" ) {
			$sth=$file_db->query('SELECT DISTINCT Nom,Prenom FROM DESSINATEUR ORDER BY Nom');
			$args = array('Nom','Prenom');
		}elseif ($type=="Genre") {
			$sth=$file_db->query('SELECT DISTINCT Nom FROM GENRE ORDER BY Nom');
			$args = array('Genres');
		}else{
			$sth=$file_db->query('SELECT DISTINCT Annee FROM MANGA ORDER BY Annee');
			$args = array('Annee');
		}

	}
	toTable($type,$sth,$args,TRUE);
  // on ferme la connexion
  $file_db=null;
  initfooter();
}
catch(PDOException $ex){
  alert("warning",$ex->getMessage());
}
?>