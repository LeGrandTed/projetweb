<?php
date_default_timezone_set('Europe/Paris');
require("connexion.php");
require("fonctions.php");
initheader();
$file_db = connect_bd();
$file_db -> setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_WARNING);
$base = file_get_contents("tableCreation.sql");
$sql = file_get_contents("insertion.sql");
$qr = $file_db->exec($base);
$qr = $file_db->exec($sql);
$file_db = null;
alert("success","La base de donnée de la Mangatech a été initialisé");
initfooter();
?>