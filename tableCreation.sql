DROP TABLE IF EXISTS MANGA;
DROP TABLE IF EXISTS AUTEUR;
DROP TABLE IF EXISTS DESSINATEUR;


CREATE TABLE IF NOT EXISTS MANGA ( 
IDmanga Number (10) PRIMARY KEY UNIQUE,
Titre Varchar (20),
NomAuteur Varchar (20),
PrenomAuteur Varchar (20),
IDauteur Number (10),
NomDessinateur Varchar (20),
PrenomDessinateur Varchar (20),
IDdessinateur Number (10),
Genre Varchar (20),
IDgenre Number (10),
Annee Number (4),
FOREIGN KEY (IDauteur) REFERENCES AUTEUR(IDauteur),
FOREIGN KEY (IDdessinateur) REFERENCES DESSINATEUR(IDdessinateur),
FOREIGN KEY (IDgenre) REFERENCES GENRE(IDgenre)
);


CREATE TABLE IF NOT EXISTS AUTEUR ( 
IDauteur Number (10) PRIMARY KEY UNIQUE,
Nom Varchar (20),
Prenom Varchar (20));

CREATE TABLE IF NOT EXISTS DESSINATEUR ( 
IDdessinateur Number (10) PRIMARY KEY UNIQUE,
Nom Varchar (20),
Prenom Varchar (20));

CREATE TABLE IF NOT EXISTS GENRE ( 
IDgenre Number (10) PRIMARY KEY UNIQUE,
Nom Varchar (20));