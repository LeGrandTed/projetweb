insert into MANGA values (1, "Berserk", "Kentarou", "Miura", 1, "Kentarou", "Miura", 1, "Action", 1, 1989);
insert into MANGA values (2, "One Piece", "Eiichiro", "Oda", 2, "Eiichiro", "Oda", 2, "Adventure", 2, 1997);
insert into MANGA values (3, "Vagabond", "Eiji", "Yoshikawa", 3, "Takehiko", "Inoue", 3, "Historical", 3, 1998);
insert into MANGA values (4, "Monster", "Naoki", "Urasawa", 4, "Naoki", "Urasawa", 4, "Mystery", 4, 1994);
insert into MANGA values (5, "Full Metal Alchemist", "Hiromu", "Arakawa", 5, "Hiromu", "Arakawa", 5, "Action", 1, 2001);
insert into MANGA values (6, "Oyasumi Punpun", "Inio", "Asano", 6, "Inio", "Asano", 6, "Slice of Life", 5, 2007);
insert into MANGA values (7, "Grand Blue", "Kenji", "Inoue", 7, "Kimitake", "Yoshioka", 7, "Comedy", 6, 2014);
insert into MANGA values (8, "Slam Dunk", "Takehiko", "Inoue", 8, "Takehiko", "Inoue", 3, "Sports", 7, 1990);
insert into MANGA values (9, "Kingdom", "Yasuhisa", "Hara", 9, "Yasuhisa", "Hara", 8, "Military", 8, 2006);
insert into MANGA values (10, "Vinland Saga", "Makoto", "Yukimura", 10, "Makoto", "Yukimura", 9, "Drama", 9, 2005);
insert into MANGA values (11, "Monogatari Series", "Isin", "Nisio", 11, "Vofan", NULL, 10, "Supernatural", 10, 2006);


insert into AUTEUR values (1, "Kentarou", "Miura");
insert into AUTEUR values (2, "Eiichiro", "Oda");
insert into AUTEUR values (3, "Eiji", "Yoshikawa");
insert into AUTEUR values (4, "Naoki", "Urasawa");
insert into AUTEUR values (5, "Hiromu", "Arakawa");
insert into AUTEUR values (6, "Inio", "Asano");
insert into AUTEUR values (7, "Kenji", "Inoue");
insert into AUTEUR values (8, "Takehiko", "Inoue");
insert into AUTEUR values (9, "Yasuhisa", "Hara");
insert into AUTEUR values (10, "Makoto", "Yukimura");
insert into AUTEUR values (11, "Isin", "Nisio");


insert into DESSINATEUR values (1, "Kentarou", "Miura");
insert into DESSINATEUR values (2, "Eiichiro", "Oda");
insert into DESSINATEUR values (3, "Takehiko", "Inoue");
insert into DESSINATEUR values (4, "Naoki", "Urasawa");
insert into DESSINATEUR values (5, "Hiromu", "Arakawa");
insert into DESSINATEUR values (6, "Inio", "Asano");
insert into DESSINATEUR values (7, "Kimitake", "Yoshioka");
insert into DESSINATEUR values (8, "Yasuhisa", "Hara");
insert into DESSINATEUR values (9, "Makoto", "Yukimura");
insert into DESSINATEUR values (10, "Vofan", NULL);


insert into GENRE values (1, "Action");
insert into GENRE values (2, "Adventure");
insert into GENRE values (3, "Historical");
insert into GENRE values (4, "Mystery");
insert into GENRE values (5, "Slice of Life");
insert into GENRE values (6, "Comedy");
insert into GENRE values (7, "Sports");
insert into GENRE values (8, "Military");
insert into GENRE values (9, "Drama");
insert into GENRE values (10, "Supernatural");